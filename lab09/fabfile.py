from fabric.api import run,env,cd,get
import glob

env.hosts = ['140.192.30.237']
env.user = 'user1317007tmp'
env.parallel = True

def clone():
    run('git clone https://kyu10@bitbucket.org/kyu10/csc299.git')
def file():
    files = ['lab06/program63.py', 'lab07/program71.py', 'lab07/program72.py', 'lab04/program41.py', 'lab04/program42.py', 'lab03/program32.py', 'lab03/program31.py', 'lab03/program33.py']
    #skipped lab02/program1.py
    with cd('csc299'):
        for file in sorted(files):
		with cd(file[:5]):
	    		run('python '+ file[6:])

    run('zip -r csc299.zip csc299')
    get('csc299.zip', 'csc299.zip')   
