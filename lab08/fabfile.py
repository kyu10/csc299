from __future__ import with_statement
from fabric.api import env, sudo, run, get, local, cd
from fabric.operations import get,put
from fabric.contrib.files import exists
import os

env.user = 'user1317007tmp'
env.password = '987654321'
env.hosts = ['140.192.30.237']
env.parallel = True
def zip_and_retrieve(folder):
    filename = folder + '.zip'
    run('zip -r %s %s' %(filename,folder))
    get(filename,filename)

def deploy_and_unzip(zipfile,folder):
    put(zipfile, zipfile)    
    if exists(folder):
        run('rm -f %s' % (folder))
    run('unzip -o %s -d %s' % (zipfile, folder))
def install_facebook_clone():
    local('wget http://www.web2py.com/examples/static/web2py_src.zip')
    put('web2py_src.zip','web2py_src.zip')
    run('unzip -o web2py_src.zip')
    with cd('web2py/applications'):
	if not exists('fb'):
	    run('mkdir fb')
	with cd('fb'):
	    run('wget  https://github.com/mdipierro/web2py-appliances/raw/master/FacebookClone/web2py.app.FacebookClone.w2p')
	    run('tar zxvf web2py.app.FacebookClone.w2p')
    with cd('web2py'):
	run('python web2py.py -i 0.0.0.0 -p 9007   -a 987654321')


	
