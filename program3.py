import os 
import csv

writing = csv.writer(open('bincount.csv', 'wb'))

file_lengths = {}

for root, dir, files in os.walk("/bin/"):
    dir_path = root.split('/')
    for file in files:
        length_size = len(file)
        if length_size in file_lengths:
            file_lengths{length_size} += 1
        else:
            file_lengths{length_size} += 1
for key, values in file_lengths.items():
    writing.writerow([key, value])
